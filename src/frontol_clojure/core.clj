(ns frontol-clojure.core
  (:gen-class)
  (:require [clojure.java.jdbc :as j]
            [frontol-clojure.queries :refer [queries_map]]
            [frontol-clojure.configs :refer [config common_config]]
            [compojure.core :refer :all]
            [compojure.route :as route]
            [cheshire.core :as cheshire]
            [ring.middleware.params :refer [wrap-params]]
            [ring.adapter.jetty :refer [run-jetty]]))

(def url-prefix "/frontol")

(defn wrap-content-type [handler content-type]
  (fn [request]
    (let [response (handler request)]
      (assoc-in response [:headers "Content-Type"] content-type))))

(defn get_data
  [shop sql_query_key & args]
  (let [shop_connect_map (merge common_config (if (get config shop)
                                                (get config shop)
                                                (throw (Exception. "Specified shop are not exist. Please check shop parameter"))))
        connection_map (-> shop_connect_map
                            (assoc :subname (str "//" (:host shop_connect_map) "/" (:database shop_connect_map)))
                            (dissoc :host :database))
        prepare-statement (if-let [sql_query ((keyword sql_query_key) queries_map)]
                            (-> connection_map
                              (j/get-connection)
                              (j/prepare-statement ((keyword sql_query_key) queries_map) {:return-keys false}))
                            (throw (Exception. "Specified sql function are not exist. Please route parameter")))]
  (try
    (j/query connection_map (reduce conj [prepare-statement] (first args)))
    (catch Exception e (throw e)))))

(def my-routes
 (routes
  (GET (str url-prefix "/:sql")
       [sql shop & args]
         (try
            (cheshire/generate-string {:fun sql :shop shop :args args :data (into [] (get_data shop sql (vals args)))})
           (catch Exception e
             (cheshire/generate-string {:error (str "caught exception: " (.getMessage e))}))))
  (route/not-found (cheshire/generate-string {:error "Error 404. This route are not exist"}))))

(defn -main
  []
  (run-jetty
    (-> my-routes
      wrap-params
      (wrap-content-type "application/json"))
    {:port 8035}))
