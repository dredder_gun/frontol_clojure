(ns frontol-clojure.configs)

(def common_config {:classname   "org.firebirdsql.jdbc.FBDriver"
                    :subprotocol "firebirdsql"
                    :port 3050
                    :user "SYSDBA"
                    :password "masterkey"
                    :encoding "WIN1251"})

(def config
  {; Номер первой транзакции для отладки 3700000
   "Партизанская" {:host "192.168.0.85"
                   :database "C:\\Frontol\\DB\\MAIN.GDB"}
   ; Номер первой транзакции для отладки 1400000
   "Комуна" {:host "192.168.2.1"
             :database "C:\\Frontol\\DB\\MAIN.GDB"}
   ; Номер первой транзакции для отладки 820000
   "Лисиха" {:host "90.188.253.48"
             :database "C:\\Frontol\\DB\\MAIN.GDB"}
   ; Номер первой транзакции для отладки 200000
   "Радищева" {:host "87.103.175.134"
               :database "E:\\bd\\MAIN.GDB"}
   ; Номер первой транзакции для отладки 1520000
   "Байкальская" {:host "192.168.5.1"
                  :database "C:\\Frontol\\DB\\MAIN.GDB"}})
