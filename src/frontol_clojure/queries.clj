(ns frontol-clojure.queries)

(def queries_map
  {:get_transactions "SELECT * FROM TranzT WHERE TranzCount >= ? AND TranzCount <= ? ORDER BY TranzCount"
   :get_nomenclatures "SELECT * FROM SprT"})
