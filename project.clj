(defproject frontol-clojure "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.8.0"]
                 [org.clojure/java.jdbc "0.7.1"]
                 [org.firebirdsql.jdbc/jaybird-jdk18 "3.0.2"]
                 [ring/ring-core "1.6.2"]
                 [cheshire "5.8.0"]
                 [ring/ring-jetty-adapter "1.5.0"]
                 [compojure "1.6.0"]]
  :main frontol-clojure.core)
